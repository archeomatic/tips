# tuto, tips *et caetera*

Cette page concerne l'utilisation d'un gestionnaire de dépôt ![https://gitlab.com/archeomatic/tips](images/gitlab.png) pour accueillir les sources nécessaires à l'édition en ligne (et en pdf) de supports des formations SIG & Stat de l'Inrap.

Cette [**page web statique**](https://archeomatic.gitlab.io/tips/) est  générée :

* à partir du dépôt ![https://gitlab.com/archeomatic/tips](images/gitlab.png)(https://gitlab.com/archeomatic/tips)
* avec les [GitLabsPages](https://docs.gitlab.com/ee/user/project/pages/) et le générateur de site web statique [Gitbook](https://www.gitbook.com/)

> Note: vous trouverez aussi dans ce dépôt d'autres tutos, tips et cie



![formation]( images/gis_training.png)

PS: vous pouvez aussi accéder à une version PDF ici  [![icon_pdf](images/pdf.png)](https://gitlab.com/archeomatic/tips/-/jobs/artifacts/master/raw/public/tips.pdf?job=pages)

... et même accéder à un diaporama ! [![presentation](images/presentation32.png)](https://archeomatic.gitlab.io/tips/slides)

