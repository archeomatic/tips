# Diagramme ternaire

archeomatic
11 octobre 2017    

Le tableau de données
On va d’abord créer un tableau avec une colonne d’identifiant et 3 variables quantitatives.

D’abord on crée 3 “vecteurs” id, var1, var2 et var3:

```R
id <-   c(1,2,3,4,5,6,7,8)
var1 <- c(1,1,3,1,2,3,1,1)
var2 <- c(2,3,2,1,2,3,1,2)
var3 <- c(3,2,1,1,2,3,2,2)
```

Puis on en fait un tableau:

```R
tri <- data.frame(id,var1,var2,var3)
```
```
id	var1	var2	var3
1	1	2	3
2	1	3	2
3	3	2	1
4	1	1	1
5	2	2	2
6	3	3	3
7	1	1	2
8	1	2	2
```

Le package ade4 et la fonction triangle.plot

On installe le package ade4:

```R
install.packages("ade4")
```

On active le package:

```R
library("ade4")
```

On se renseigne sur la fonction triangle.plot

```R
help(triangle.plot)
```

Attention le tableau source ne doit comporter que 3 colonnes ??? qui correspondent aux 3 variables continues à placer
=> Il ne faut donc pas de colonne identifiant (genre numéro de faits ou d’objet!!)

=> Ou il faut spécifier les 3 colonnes qui contiennent les 3 variables continues à prendre en compte ((les colonnes 2 à 4))

Allez, on teste !

```R
triangle.plot(ta = tri[2:4])
```
![diagramme ternaire](exemples/images/1_diagramme_ternaire.png)

… et la ca fonctionne !!! C’est kiléplufor ?

Résultats: et voila avec en plus un triangle de localisation, à la façon vignette de localisation dans QGIS : un bonheur en terme de justesse graphique car chaque axe doit commencer théoriquement à 0

Bon après il y a pleins de paramètres possibles:

```R
triangle.plot(ta = tri[2:4],
              addmean = T, # ajouter les moyennes et le barycentre
              label = row.names(tri), # ajouter les identifiants aux points
              clabel = 0.8, # Attention: il faut définir une taille de police (0.8 = 80%)
              draw.line = F # on peut même virer le carroyage
              )
```
![diagramme ternaire](exemples/images/2_diagramme_ternaire_zoom.png)

Bon bah c’était pas si dur le diagramme triangulaire ;)