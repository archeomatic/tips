# Premiers pas avec Gitlab

Aller maintenant que l'on sait tout.. du moins se qu'il faut savoir on se lance !

et pour cela il va falloir:

- se **créer un dossier local** contenant tous les projets git 
- **installer le [logiciel git](http://msysgit.github.io/)** pour pouvoir gérer tout le bouzin entre version/dépôt locale et version/dépôt en ligne.
- se familiariser avec quelques notions de git et l'usage de la ligne de commande.

## Création d'une version locale du projet

1. Avant tout, Installer le [logiciel git](http://msysgit.github.io/). Il vous donne accès à de **nouvelles options via un clic droit quand vous êtes dans un dossier**. ![git](images/01_clicdroit_git.png)

Le premier ouvre une interface graphique et le deuxième une fenêtre d'invite de commande qui va nous servir pour la suite.

2. Créer un dossier pour contenir tous vos projets git (par exemple C:/ git)

3. Ensuite, se rendre à la racine du projet sur gitlab.inrap.fr par exemple à la racine du projet de la formation perf: https://gitlab.inrap.fr/formation/perf.

   ![attention](images/attention.png)  *Ce lien vers le projet est une ancienne version pour la démonstration...*

   grâce au bouton [Clone↓] on peut copier l'adresse http du projet en cliquant sur l'icône dédié:

   ![clone url](images/02_clone_projet.png)

4. Revenir dans votre dossier en local [gitlab_inrap] et faire un clic droit => ![bash](images/icon_bash.png).

   Une fenêtre d'invite de commande s'ouvre avec une ligne qui commence par un `$`:

   - taper `git clone  `puis faire un clic droit => *Paste*

   - Valider avec *[Entrée]*

     ![bash_git_clone](images/03_bash_git_clone.png)

     **=> Nous avons créé un clone de ce projet en local !**

Vous pouvez d'ailleurs le constater avec la création dans votre dossier **[gitlab_inrap]** d'un nouveau sous-dossier **[perf]** contenant la même arborescence que la version en ligne avec en plus un dossier **[.git]**

## Modifier en local et verser en ligne

Imaginons que vous modifier le pas à pas de la formation c'est à dire le fichier *C:\gitlab_inrap\perf\pas_a_pasperf1_analyse_spatiale_pas_a_pas.md*  avec un éditeur de markdown.

Une fois les modifications faites et enregistrées.

Dans le dossier local [C:\gitlab_inrap\perf\] faites un clic droit => ![bash](images/icon_bash.png) pour travailler avec **git** dans le projet en cours.

1. comparer le dépôt local et en ligne en faisant un `git status`

   ![git_status](images/04_bash_git_status.png)

=> les lignes en rouge indiquent en rouge le(s) fichier(s) modifié(s) en local non présents en ligne.

2. **Valider les fichiers modifiés** en local afin que git le surveille avec la commande `git add .`

   Attention: les fichiers modifiés sont juste passés de l'état "inconnu pour git" à "surveiller par git".

3. **Faire un *commit*** c'est à dire créer une étape de modification avec un commentaire grâce à la commande `git commit -m "commenter ici la modification effectuée"`

4. **Faire un push** pour envoyer la modification en ligne avec la commande `git push`

   > Note: la première fois que l'on fait un push on utilise la commande `git push -u origin master`indiquant:
   >
   > - avec l'option `-u` que les prochain *push* n'auront pas à être paramétrés.
   > - avec les paramètres `origin master` on indique depuis quel dossier en local on travaille et vers quelle branche en ligne les modifications vont être faites.

5. si vous avez un message d'erreur en jaune comme ci-dessous c'est qu'il y a un problème entre la version en ligne et en local il vous dit de faire un ```git pull``` avant de refaire la manip depuis le 1.
   ![git_bash_pull](images/07_bash_git_pull.png)

## Créer une clé SSH pour se connecter plus facilement

en gros, il faut:

1. se rendre dans son dossier [c:/gitlab_inrap] => clic droit > ![bash](images/icon_bash.png)
2. Taper la commande  `ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub` pour générer une clé SSH privée.
3. pour vérifier quelle existe taper `cat ~/.ssh/id_rsa.pub` si un texte commençant par `ssh-rsa 9LETTRES nom_de_votre_ordi`s'affiche c'est bien !
4. se rendre dans l'interface de gitlab : Clic sur votre avatar (en haut à droite)> Paramètres
5. A gauche est apparu le menu *Paramètres de l'utilisateur* choisir ![cle_ssh](images/05_cle_ssh.png)
6. Dans la fenêtre **Key** copier la clé SSH préalablement copiée depuis la fenêtre Bash (ou depuis le fichier  **id_rsa.pub** qui se trouve dans **C:\Users\\*nom_d_utilisateur*\\.ssh ** édité dans Notepad)
7. Enfin, cliquer sur le bouton [Ajouter une clef]
8. Maintenant il suffit de suivre la procédure depuis [création d'une version locale du projet](#création d'une version locale du projet)  mais en copiant le lien SSH depuis gitlab (au lieu de https)
9. **attention c'et un peu capricieux j'y suis arrivé mais je ne sais plus exactement comment ;( **