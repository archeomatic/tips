# Parties éliminées de perf 1

**Observation : les relations entre Faits et US**

Dans QGIS, propriétés du projet pour observer la définition de la relation entre les deux tables. ![relation](images/image020.png)

![relations](images/image021.png)

![attention](images/attention.png) Nous observons ici des relations **qui ont déjà été paramétrées dans le projet** comme nous venons de le faire dans la partie précédente [1.2.3. Les relations dans QGIS](# 1-2-3-les-relations-dans-qgis) cela n'a rien à voir avec le format de données, ici .sqlite

**Mettre en application la relation dans les tables/couches**: Pour cela, il faut agir sur les Propriétés des champs des tables liées. PAS CLAIR ?!:

* Depuis la **Table Fille** **tabus** :

  1. Clic droit ⇒ Propriétés ⇒ ![form](images/icon_form.png)Formulaire d’attributs

  2. Dans *Couches disponibles* cliquer sur le champs correspondant à la **clé étrangère** ![fk](images/icon_fk.png), ici **"num_fait"**
  3. Dans les paramètres à droite, Sous menu *Type d'outil*  ⇒ *Référence de la relation*
  4. Dans *Display expression* choisir **"num_fait"** (ou son Alias *numéro*) et dans *Relation* choisir **fait_us**

  ![proprietes_couche_formulaire](images/image022.png)

Pour consulter les enregistrements, à partir de la table attributaire de **tabUS**:

* Si nécessaire, cliquer sur l'icône formulaire ![form](images/icon_form.png) en bas à droite
* Puis **au bout de la rubrique [Fait] cliquer sur l'icône  formulaire ![form](images/icon_form.png)** (voir flèche rouge ci-dessous) pour afficher l'enregistrement du fait à laquelle appartient l’US 

![formulaire_ampoule](images/image023.png)

Maintenant que nous avons vu ce que l'on peut faire, au travail !

> *Exercice dirigé..*
>
> **Objectif:** Reconstruire une BDD relationnelle SQLite à partir de couches (shapefiles)  et de tables (.xls)
>
> 
>
> **Pas à Pas:**
>
> * **Étape 1: Créer une BDD vierge:**
>  * Partir d'un nouveau projet QGIS
>   * Si nécessaire, Afficher le panneau Explorateur (Menu Vue ⇒ Panneaux ⇒ Explorateur)
>   * Clic droit sur **Spatialite** ⇒  **"Créer une base données"**
>   * créer un fichier sqlite **Saran_BDD.sqlite**
>
> ![explorateur_spatialite](images/image024.png)
>
> * **Étape 2: Importer des couches:**
>   * Ouvrir le **gestionnaire de Bases de données** via Menu [Base de données] ⇒ Gestionnaire de Base de Donnée (= *DB Manager*) ou via l'icône ![icon_DBmanager](images/icon_DB.png)
>
> ![Menu_DBmanager](images/image025.png)
>
>  1. Dans *Fournisseur de données* ⇒ *Spatialite* vérifier la présence de la BDD **Saran_BDD.sqlite** et **cliquer dessus pour vous connecter**.
>
>     > ![attention](images/attention.png)Parfois les tables n’apparaissent pas, dans ce cas faire un rafraichissement (F5) ou ![refresh](images/icon_F5.png)
>
>    2. **Importer une couche ou un fichier** 
>
> ![DBManager](images/image026.png)
>
> ![DBManager_Import](images/image027.png)
>
> 1. Dérouler le menu *Saisie* pour chercher la couche (ici un shapefile) à importer
> 2. Donner un nom à la table qui va être créée: **tabfait**
> 3. **Créer index spatial** permet d’améliorer les performances d’affichage:Indispensable
> 4. Valider avec [OK]

> ![exo](images/icon_exo.png)**Exercice en Autonomie:**
>
> * Importer le shape **F108633_us** dans la BDD Sqlite **Saran_BDD.Sqlite** et la nommer **tabUS** .
> * Créer les relations dans QGIS entre **tabFait** et **tabUS** .