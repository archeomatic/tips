# Mise en ligne d'un site statique d'après des documents en markdown stockés sur gitlab

Vous avez déjà créé/alimenté un dépôt gitlab avec des tonnes de documents.md, le tout bien rangé ! Il est temps de passer aux choses sérieuses : mettre en valeur ce travail magnifique ! Le mettre en ligne dans un site web *dit* statique. 

Ceci est un ensemble de notes pour tenter une mise en ligne sous forme de site web statique avec le systeme de Gitlab Pages et gitbook.

Ressources:

*  [documentation de GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
*  [publier un website avec GitLab Pages (video eng)](https://youtu.be/TWqh9MtT4Bg)



:warning: Ceci est un travail en cours (*work in progress*) les démarches ci-dessous ne sont pas encore totalement effectives sur **gitlab.inrap.fr** mais on y travaille avec la DSI... en attendant les tests sont effectués sur un compte **gitlab.com**



## Créer le site web statique

Le plus simple -quand on part de Zéro- est de partir d'un exemple que l'on *fork* (on en crée une  fourche, une branche) comme [proposé (en anglais) dans la documentation de GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/getting_started/fork_sample_project.html): 

1. Faire un fork du projet modèle:

   * se rendre sur la [page 2 des exemples de Pages GitLab](https://gitlab.com/pages?page=2)
   * cliquer sur **gitbook**
   * cliquer sur le bouton ![fork](images/bouton_fork.png)pour faire un fork du projet exemple et sélectionner votre espace de noms (vous vous êtes identifié sur gitlab auparavant bien sûr!)
   * Bravo vous avez dorénavant un projet **gitbook** dans votre espace GitLab !

2. Casser la relation entre le projet "forké" et le projet principal:

   * Une fois dans le projet: Projet > Paramètres > Général
   * Dérouler la page jusqu'à la rubrique **Advanced** et cliquer sur [Etendre]
   * Dérouler la page jusqu'à [Remove fork relationship] et cliquer sur le bouton idoine.

   >  note: le logiciel me demande de confirmer l'action en tapant `gitbook` puis [Confirm]

   * Une fois revenu sur le haut de la page des paramètres généraux du projet donner un nom au projet si vous voulez en changer (pour l'exemple je garde gitbook)

3. Intégration et livraison continue (CI/CD en anglais) Pipelines > [Enable shared runners] 

   > Note: A ne faire qu'une fois, la 1ère fois que l'on fait un site..

4. Pour tester modifier le fichier Readme.md en ajoutant Hello world ! dans les premières lignes.

5. Intégration et livraison continue > Pipelines > cliquer sur le Bouton [Pages]

6. Aller dans les Paramètres > Pages et regarder l'url de votre page web statique: tester 

   > :warning: **Attention** la première fois cela peut mettre plusieurs minutes  -une 10aine pour moi- avant de voir la page !

## Paramétrer le site avec votre nom  d'utilisateur: moi.gitlab.io/mon_projet

1. Renommer le dépôt (repository)

   * Paramètres du projet > Rename Repository
   * En profiter pour renommer le projet lui-même
   * Retourner sur Paramètres > pages et vérifier la nouvelle url

2. Recréer le lien vers le css

   * Éditer le fichier .config.yml
   * à la ligne baseurl: "blablabla" supprimer blablabla
   * Actualiser la page web !

## Ce qu'il faut retenir

 Pour faire un site web statique, il faut obligatoirement certains **fichiers à la racine du projet**:

 > Note: PAS DE PANIQUE, il  suffit la plupart du temps de récupérer des fichiers existant et éventuellement de changer quelques paramètres.
   >
   > Note: certains fichier existent déjà dans le projet 

   * un fichier **.gitlab-ci.yml** (langage html) qui sera le fichier qui générera le site statique.

   >  C'est dans ce fichier  sous forme de script que l'on indique à gitlab les actions à effectuer (par exemple: l’installation de l'extension gitbook, et la construction du siteweb statique avec ce dernier mais aussi la création d'un pdf). 
   >
   >  On peut voir ce fichier comme un manifeste une sorte de feuille de route pour l’exécution de tâches reliées entre elles dans un *pipeline*. Cette suite d'action sera effectuée à chaque *commit* (changement dans le projet) .
   >
   >  un bon exemple de fichier [gitlab-ci.yml]() qui génère page web et pdf (entre autre)

   *  un fichier **.gitignore** qui contient les fichiers qui doivent être ignorés lors des *commit*

   > On va par exemple lui dire d'ignorer les pdf ou les fichiers qu'il a lui même créés auparavant.

   *  un fichier **README.md** qui existe déjà mais qui a avoir ici aussi la fonction de page de garde du site web statique.

   > Ce fichier existe déjà il aura juste une fonction supplémentaire.

   *  un fichier **SUMMARY.md**  qui comme son nom l'indique contient le sommaire sous forme de liste avec des liens vers les documents .md a rassembler sur le site web.

   En outre, il faut activer **l'intégration et livraison continue** plus communément appelée **CI/CD** (*Continuous Integration / Continuous Delivery*) qui est le service qui va a chaque changement exécuter les ordres 

   > Tout ceci se fait dans le projet via le menu ![integration_et_livraison_pipelines](images/06_pipeline.png)
   >
   > La première fois  il faut activer/déployer la tâche "Pages" en cliquant sur ![icon_pipeline](images/icon_pipeline.png) Pages. pour pouvoir suivre l'éxecution du programme et (tenter de) comprendre les éventuelles erreurs... en espérant que cela finisse par le message (en vert) ```Job succeced```



   *  CI/CD Pipelines il faut activer/déployer la tâche "Pages"
   *  l'adresse du site est https://username.gitlab.io/mon_projet_de_website



***Work in progress***

* on peut ajouter à la racine un fichier **book.json** qui va contenir le paramétrages des sorties html (page web statique) et surtout du pdf, générés par le fichier **gitlab-ci.yml**