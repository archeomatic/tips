# Écrire un support de formation en markdown

La documentation présente dans GitLab est écrite en [**markdown**](https://fr.wikipedia.org/wiki/Markdown) un langage de balisage avec une syntaxe relativement simple permettant d'écrire très simplement un texte avec une mise en forme minimale. les fichiers enregistrés ont l'extension **.md**

## Mise en bouche

Avec le langage **markdown** on peut très simplement:

* faire des niveaux de titre:

># Titre 1
>
>## Titre 2
>
>### Titre 3
>
>```
># Titre 1
>## Titre 2
>### Titre 3
>````

* Mettre des mots en **gras** et en *italique* voir ~~barrés~~

> ````
> Mettre des mots en **gras** et en *italique* voir ~~barrés~~
> ````

* Faire des listes:

> * point 1
> * point 2
>   * point 2.1
>   * point 2.2
>
> 1. d'abord
> 2. ensuite
> 3. enfin
>
> ````
> * point 1
> * point 2
> 	* point 2.1
> 	* point 2.2
> 	
> 1. d'abord
> 2. ensuite
> 3. enfin
> ```

* insérer des liens vers [un super blog](https://archeomatic.wordpress.com/)

> ```
> insérer des liens vers [un super blog](https://archeomatic.wordpress.com/)
> ```

* ou insérer une image ![icon_gitlab](images/gitlab.png)

> ```
> ou insérer une image ![icon_gitlab](images/gitlab.png)
> ```

* et même des tableaux !

>| ID   | nom       | prénom | taille |
>| ---- | --------- | ------ | ------ |
>| 1    | la mouche | zobi   | 10     |
>| 2    | michel    | michel | 180    |
>
>```
>| ID   | nom       | prénom | taille |
>| ---- | --------- | ------ | ------ |
>| 1    | la mouche | zobi   | 10     |
>| 2    | michel    | michel | 180    |
>```

Un peu déconcertant les premières minutes... on se met très vite à prendre du plaisir à écrire et mettre en page juste avec un clavier !

Bon d'accord si vous insistez on peut mettre des émoticônes chat :cat:, voire des têtes de mort ! :skull: à vous de trouver comment [?!](https://gist.github.com/rxaviers/7360908)

![atention](images/attention.png)Malheureusement ces *emojis* disparaissent lors de la mise en ligne  sous forme de page web statiques via gitlab pages.. A n'utiliser que pour les documents qui n'ont pas vocation à devenir une page web.

## Pour aller plus loin

Ci-dessous quelques notes sur la syntaxe markdown [passer directement à la suite si vous êtes déjà convaincu ou que vous êtes préssés !](#range-ta-chambre!)
Outre la syntaxe que vous trouverez partout sur le web, ci-dessous quelques notes perso en plus... ↓

### caractères spéciaux (ASCII)

Tous les caractères spéciaux peuvent être écrit grace au ASCII. Il suffira en restant appuyé sur la touche [Alt] 

> par exemple `Alt+25`donnera ↓ et `Alt+3`donnera ♥
> Vous trouverez les [codes ASCII](http://sebastienguillon.com/test/jeux-de-caracteres/windows-ascii-fr.html) sur la toile.

### insérer des liens 

Insérer un lien dans un texte en markdown est ridiculement simple, il suffit de l'écrire !
par exemple `https://archeomatic.wordpress.com` s'affichera https://archeomatic.wordpress.com
Pour ne pas surcharger le texte avec le lien on peut aussi écrire `[texte à afficher en bleu souligné](adresse_du_lien)`. ainsi `[mon blog](https://archeomatic.wordpress.com)` s'affichera comme ceci:  [mon blog](https://archeomatic.wordpress.com)

### Insérer des images

Insérer une image en markdown revient à insérer un lien vers une image il suffit de mettre
`![texte à afficher si pas d'image](lien_vers_l'image)`

> Les images utilisées peuvent soient déjà être en ligne soient stockées dans le dépôt.
>
> ![attention](images/attention.png) Le redimensionnement et la mise en page des images n'est pas gérée nativement avec le markdown il est fortement conseillé de préparer le images en amont !

### et une image cliquable ?

No soucis il suffit de retenir que ce qui est cliquable doit être entre crochets.

 Donc `[![icon_gitlab](images/gitlab.png)](https://gitlab.com/formationsig)` donnera [![icon_gitlab](images/gitlab.png)](https://gitlab.com/formationsig) c'est à dire une image cliquable pour accéder au dépôt gitlab pour les formations !!

### créer des liens dans un document

On peut avoir besoin de créer des liens à l'intérieur d'un document pour faire référence à un paragraphe dans une table des matières en début de document par exemple ou même pour mettre un peu de dynamisme dans le support (on pourrait même très simplement faire référence au résultat d'un exercice de cette façon.)

>  Rappel: En markdown les liens sont tous écrits sous la forme `[lien](adresse-du-lien)`.

Dans GitLab toutes les entêtes de paragraphe (celles qui sont précédées de 1 ou plusieurs #) obtiennent automatiquement un identifiant sous la forme `#mon-entete-en-minuscule-et-les-espaces-remplaces-par-un-tiret`.

La manière la plus simple pour faire référence à une entête paragraphe appelée **Introduction ** est donc d'y faire référence directement sous la forme `[lien vers l'intro](#Introduction)`. Ce qui donne [lien vers l'intro](#introduction)

Il faut respecter quelques règles:

- le lien est constitué d'un dièse `#` (quel que soit le niveau de titre un seul dièse suffit!)
- il n'y a pas d'espace entre le dièse et le nom
- le nom du titre doit être écrit en **minuscule** et les **espaces remplacés par un tiret `-`**
- Attention: les accents et caractères spéciaux ne sont pas pris en compte ! dans ce cas le plus simple est de récupérer le nom du lien dans gitlab par exemple.

> Pour récupérer la dénomination d'un lien dans GitLab il suffit de survoler l'entête dont on veut récupérer le lien , un icône lien apparaît alors sur la gauche de cette entête. Faire un clic droit => Copier l'adresse du lien. Puis coller le lien.
>
> ![lien](images/99_lien_interne.png)
>
> Le lien enregistré  et collé est : https://gitlab.inrap.fr/formation/perf/blob/master/memoSQL/memoSQL.md#requ%C3%AAte-dune-relation-de-1-%C3%A0-n
>
> Remarquez:
>
> - la mise en forme en minuscule
> - le remplace des espaces (mais aussi des `.`et des `&` s'il y en avait eut)
> - le remplacement de l'encodage des caractères accentués 
>   Note: au final on peut garder le lien en relatif  est supprimer toute la partie https://blabla
>   ainsi pour **faire référence à ce paragraphe spécifique** du document *memoSQL.md* contenu dans un dossier *[memosql]* depuis le fichier *README.md* à la racine du projet, il suffit d'écrire :
>   pour accéder directement au paragraphe sur les `[relations 1 à n](/memoSQL/memoSQL.md#requ%C3%AAte-dune-relation-de-1-%C3%A0-n)`
>   qui apparaîtra ainsi:
>   pour accéder directement au paragraphe sur les [relations 1 à n](/memoSQL/memoSQL.md#requ%C3%AAte-dune-relation-de-1-%C3%A0-n)



## Range ta chambre !

Dans ![gitlab](/images/gitlab.png) toute la documentation écrite est au format **.md** dont le fichier central README.md qui s'affiche lorsqu'on accède au dépôt (et qui servira de page de garde à notre site web statique, on le verra plus tard..). 

Il est conseillé d’organiser son dépôt toujours de la même façon pour s'y retrouver:

1. Faire autant de dossier que d'ensemble cohérent de documents (par exemple un dossier par section/chapitre du support de formation ), lui donner **un nom court et explicite**
2. Dans ce [dossier] mettre les documents en **markdown** avec eux aussi des **noms courts, explicites et...ordonnés**, il est bienvenu par exemple de commencer par un chiffre: ```01_intro.md, 02_presentation_qgis, 03_edition```
3. Ce [dossier] contiendra aussi un sous-dossier [images] contenant toutes les images utilisées dans les documents du même dossier. Ainsi quel que soit le document en **markdown** du dépôt le chemin vers les images sera toujours ```[legende](images/nom_d_image.png)```
4. Enfin dernier conseil d'amis :smirk: : il peut être malin pour nommer les images de faire comme pour les documents.md c'est à dire leur donner des **noms courts, explicites et...ordonnés !**. Rappellez vous que le chemin des images sera tapé à la main...

