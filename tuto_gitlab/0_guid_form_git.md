gitlab.inrap.fr à l'usage des formateurs
======
ceci est un documents rassemblant les manips courantes et les informations minimales pour s'en sortir avec le gitlab de l'inrap à l'usage des formateurs.

# Introduction

## GIT c'est quoi ?

**GitLab** est un gestionnaire de projets de développements collaboratifs. Il intègre notamment une panoplie d’outils visant à faciliter les différents aspects liè au développement d’une application que sont la gestion des versions du code,** la collaboration entre plusieurs contributeurs, la documentation et le partage du projet**. [source: Git(lab) pour bien commencer de l'Univ. de Lille](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&ved=2ahUKEwjIj8H34_jmAhVCJFAKHc0iCYAQFjACegQICRAC&url=http%3A%2F%2Fwww.fil.univ-lille1.fr%2F~routier%2Fenseignement%2Flicence%2Fpoo%2Ftdtp%2Fgitlab.pdf&usg=AOvVaw0uQTnuQvAceoDgoovu5wmp)

Son usage peut aussi être **dévoyé pour servir de dépôt de sources (images et textes) qui forment des supports de formations accessibles en lignes dont les mises à jour peuvent être mises à dispositions en temps réel et la possibilité de les télécharger en pdf**.

> Les exemples qui ont inspirés cette démarches sont (https://larmarange.github.io/analyse-R/) qui est basé sur le [dépôt GitHub](https://github.com/larmarange/analyse-R) (un concurent de GitLab) et (https://juba.github.io/tidyverse/) basé sur un dépôt [Github](https://github.com/juba/tidyverse) lui aussi

##  GitLab.inrap.fr c'est quoi ?

La DSI de l'Inrap a mis en place en 2019 une instalation de Gitlab propre à l'inrap ce qui implique:

- d'être connecté au réseau inrap pour pouvoir y accéder à l'adresse (https://gitlab.inrap.fr/)

- de s'identifier pour pouvoir participer à un projet gitlab.inrap.fr (pas utile pour de la simple consultation ou téléchargement)

  > Pour s'identifier se rendre depuis son poste connecté au réseau inrap à l'adresse (https://gitlab.inrap.fr/users/sign_in):
  >
  > - Cliquer sur le bouton [Authentification Gitlab] en bas à droite.
  >
  > - Vous accédez à la page d'authentification cas2 de l'inrap (la même que pour l'intranet ou d'autres services internes)
  > - Renseignez votre identifiant de sessions inrap (première lettre prénom + nom) et mot de passe (le même que votre session windows)
  > - Voilà !

- d'être invité par le propriétaire d'un projet pour alimenter, modifier un projet gitlab.inrap.fr

## Le groupe formation et les projets

Un groupe **formation** a été créé et contient des projets concernant les formations liées au déploiement des SIG et des Statistiques à l'Inrap.

> :warning: En attendant quelques paramétrages du côté serveur de l'inrap, pour que gitlab.inrap.fr soit totalement opérant (le dépôt fonctionne mais pas la transformation en site web statique ni la création automatique de pdf) on peut:
>
> * tester le dépôt des sources sur Gitlab.com (https://gitlab.com/formationsig) qui contient autant de projets que de formations en cours de transformation.
> * accéder au site web statique (https://formationsig.gitlab.io/toc/), un "portail" qui permet d'accéder aux versions web des formations sus-mentionnées.

Un projet est un ensemble de dossiers et de fichiers rassemblés dans un lieu et accompagné d'un fichier **README.md** qui s'affiche en bas de la page et qui permet d'expliquer le contenu du dépôt.

##  Mais pourquoi utiliser Git ?

On ne vous avez pas tout dit pour ne pas vous faire peur... mais GIT c'est beaucoup plus que ça. En effet c'est un DCVS c’est-à-dire un système de versions concurrentes décentralisé.

Pour simplifier **chaque participant à un projet peut avoir une version locale d'un projet, travailler dessus puis proposer de la fusionner au projet principal**.

>Pour comprendre ce qu'est un [DCVS]](http://yannesposito.com/Scratch/fr/blog/2009-11-12-Git-for-n00b/) et pour démarer tout seul ce [petit guide](http://rogerdudler.github.io/git-guide/index.fr.html)