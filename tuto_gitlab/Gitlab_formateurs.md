gitlab.inrap.fr à l'usage des formateurs
======
ceci est un documents rassemblant les manips courantes et les informations minimales pour s'en sortir avec le gitlab de l'inrap à l'usage des formateurs.

[toc]

# Introduction

## GIT c'est quoi ?

**GitLab** est un gestionnaire de projets de développements collaboratifs. Il intègre notamment une panoplie d’outils visant à faciliter les différents aspects liè au développement d’une application que sont la gestion des versions du code,** la collaboration entre plusieurs contributeurs, la documentation et le partage du projet**. [source: Git(lab) pour bien commencer de l'Univ. de Lille](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&ved=2ahUKEwjIj8H34_jmAhVCJFAKHc0iCYAQFjACegQICRAC&url=http%3A%2F%2Fwww.fil.univ-lille1.fr%2F~routier%2Fenseignement%2Flicence%2Fpoo%2Ftdtp%2Fgitlab.pdf&usg=AOvVaw0uQTnuQvAceoDgoovu5wmp)

Son usage peut aussi être **dévoyé pour servir de dépôt de sources (images et textes) qui forment des supports de formations accessibles en lignes dont les mises à jour peuvent être mises à dispositions en temps réel et la possibilité de les télécharger en pdf**.

> Les exemples qui ont inspirés cette démarches sont (https://larmarange.github.io/analyse-R/) qui est basé sur le [dépôt GitHub](https://github.com/larmarange/analyse-R) (un concurent de GitLab) et (https://juba.github.io/tidyverse/) basé sur un dépôt [Github](https://github.com/juba/tidyverse) lui aussi

##  GitLab.inrap.fr c'est quoi ?

La DSI de l'Inrap a mis en place en 2019 une instalation de Gitlab propre à l'inrap ce qui implique:

- d'être connecté au réseau inrap pour pouvoir y accéder à l'adresse (https://gitlab.inrap.fr/)

- de s'identifier pour pouvoir participer à un projet gitlab.inrap.fr (pas utile pour de la simple consultation ou téléchargement)

  > Pour s'identifier se rendre depuis son poste connecté au réseau inrap à l'adresse (https://gitlab.inrap.fr/users/sign_in):
  >
  > - Cliquer sur le bouton [Authentification Gitlab] en bas à droite.
  >
  > - Vous accédez à la page d'authentification cas2 de l'inrap (la même que pour l'intranet ou d'autres services internes)
  > - Renseignez votre identifiant de sessions inrap (première lettre prénom + nom) et mot de passe (le même que votre session windows)
  > - Voilà !

- d'être invité par le propriétaire d'un projet pour alimenter, modifier un projet gitalab.inrap.fr

## Le groupe formation et les projets

Un groupe **formation** a été créé et contient des projets concernant les formations liées au déploiement des SIG et des Statistiques à l'Inrap.

> Par exemple tout agent inrap peut accéder aux sources du projet concernant les formation SIG (pour l'instant  SIG perfectionnement 1: Analyse spatiale) à l'adresse (https://gitlab.inrap.fr/formation/perf) et ...bientôt au site web..
>
> ![attention](images/attention.png)En attendant quelques paramétrages du côté serveur de l'inrap on peut tester le dépôt des sources sur GitLab.com(https://gitlab.com/archeomatic/formation)  et le site web statique (https://archeomatic.gitlab.io/formation/)

Un projet est un ensemble de dossiers et de fichiers rassemblés dans un lieu et accompagné d'un fichier README.md qui s'affiche en bas de la page et qui permet d'expliquer le contenu du dépôt.

##  Mais pourquoi utiliser Git ?

On ne vous avez pas tout dit pour ne pas vous faire peur... mais GIT c'est beaucoup plus que ça. En effet c'est un DCVS c’est-à-dire un système de versions concurrentes décentralisé.

Pour simplifier **chaque participant à un projet peut avoir une version locale d'un projet, travailler dessus puis proposer de la fusionner au projet principal**.

>Pour comprendre ce qu'est un [DCVS]](http://yannesposito.com/Scratch/fr/blog/2009-11-12-Git-for-n00b/) et pour démarer tout seul ce [petit guide](http://rogerdudler.github.io/git-guide/index.fr.html)

# Premiers pas avec Gitlab
Aller maintenant que l'on sait tout.. du moins se qu'il faut savoir on se lance !

et pour cela il va falloir:

- se **créer un dossier local** contenant tous les projets git 
- **installer le [logiciel git](http://msysgit.github.io/)** pour pouvoir gérer tout le bouzin entre version/dépôt locale et version/dépôt en ligne.
- se familiariser avec quelques notions de git et l'usage de la ligne de commande.

### Création d'une version locale du projet

1. Avant tout, Installer le [logiciel git](http://msysgit.github.io/). Il vous donne accès à de **nouvelles options via un clic droit quand vous êtes dans un dossier**. ![git](images/01_clicdroit_git.png)

Le premier ouvre une interface graphique et le deuxième une fenêtre d'invite de commande qui va nous servir pour la suite.

2. Créer un dossier pour contenir tous vos projets git (par exemple C:/ git)

3. Ensuite, se rendre à la racine du projet sur gitlab.inrap.fr par exemple à la racine du projet de la formation perf: https://gitlab.inrap.fr/formation/perf.

   grâce au bouton [Clone↓] on peut copier l'adresse http du projet en cliquant sur l'icône dédié:

   ![clone url](images/02_clone_projet.png)

4. Revenir dans votre dossier en local [gitlab_inrap] et faire un clic droit => ![bash](images/icon_bash.png).

   Une fenêtre d'invite de commande s'ouvre avec une ligne qui commence par un `$`:

   - taper `git clone  `puis faire un clic droit => *Paste*

   - Valider avec *[Entrée]*

     ![bash_git_clone](images/03_bash_git_clone.png)

     **=> Nous avons créé un clone de ce projet en local !**

Vous pouvez d'ailleurs le constater avec la création dans votre dossier **[gitlab_inrap]** d'un nouveau sous-dossier **[perf]** contenant la même arborescence que la version en ligne avec en plus un dossier **[.git]**

### Modifier en local et verser en ligne

Imaginons que vous modifier le pas à pas de la formation c'est à dire le fichier *C:\gitlab_inrap\perf\pas_a_pasperf1_analyse_spatiale_pas_a_pas.md*  avec un éditeur de markdown.

Une fois les modifications faites et enregistrées.

Dans le dossier local [C:\gitlab_inrap\perf\] faites un clic droit => ![bash](images/icon_bash.png) pour travailler avec **git** dans le projet en cours.

1. comparer le dépôt local et en ligne en faisant un `git status`

   ![git_status](images/04_bash_git_status.png)

=> les lignes en rouge indiquent en rouge le(s) fichier(s) modifié(s) en local non présents en ligne.

2. **Valider les fichiers modifiés** en local afin que git le surveille avec la commande `git add .`

   Attention: les fichiers modifiés sont juste passés de l'état "inconnu pour git" à "surveiller par git".

3. **Faire un *commit*** c'est à dire créer une étape de modification avec un commentaire grâce à la commande `git commit -m "commenter ici la modification effectuée"`

4. **Faire un push** pour envoyer la modification en ligne avec la commande `git push`

   > Note: la première fois que l'on fait un push on utilise la commande `git push -u origin master`indiquant:
   >
   > - avec l'option `-u` que les prochain *push* n'auront pas à être paramétrés.
   > - avec les paramètres `origin master` on indique depuis quel dossier en local on travaille et vers quelle branche en ligne les modifications vont être faites.

5. si vous avez un message d'erreur en jaune comme ci-dessous c'est qu'il y a un problème entre la version en ligne et en local il vous dit de faire un ```git pull``` avant de refaire la manip depuis le 1.
![git_bash_pull](images/07_bash_git_pull.png)

### Créer une clé SSH pour se connecter plus facilement

en gros, il faut:
1. se rendre dans son dossier [c:/gitlab_inrap] => clic droit > ![bash](images/icon_bash.png)
2. Taper la commande  `ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub` pour générer une clé SSH privée.
3. pour vérifier quelle existe taper `cat ~/.ssh/id_rsa.pub` si un texte commencant par `ssh-rsa 9LETTRES nom_de_votre_ordi`s'affiche c'est bien !
4. se rendre dans l'interface de gitlab : Clic sur votre avatar (en haut à droite)> Paramètres
5. A gauche est apparu le menu *Paramètres de l'utilisateur* choisir ![cle_ssh](images/05_cle_ssh.png)
6. Dans la fenêtre **Key** copier la clé SSH préalablement copiée depuis la fenêtre Bash (ou depuis le fichier  **id_rsa.pub** qui se trouve dans **C:\Users\\*nom_d_utilisateur*\\.ssh ** édité dans Notepad)
7. Enfin, cliquer sur le bouton [Ajouter une clef]
8. Maintenant il suffit de suivre la procédure depuis [création d'une version locale du projet](#création d'une version locale du projet)  mais en copiant le lien SSH depuis gitlab (au lieu de https)
9. **ça ne fonctionne pas ! nis sur gitlab.inrap.fr ni sur gitlab ;( **



# Écrire un support de formation en markdown

La documentation présente dans GitLab est écrite en [**markdown**](https://fr.wikipedia.org/wiki/Markdown) un langage de balisage avec une syntaxe relativement simple permettant d'écrire très simplement un texte avec une mise en forme minimale. les fichiers enregistrés ont l'extension **.md**

## Mise en bouche

Avec le langage **markdown** on peut très simplement:

* faire des niveaux de titre:
># Titre 1
>## Titre 2
>### Titre 3
>```
># Titre 1
>## Titre 2
>### Titre 3
>````

* Mettre des mots en **gras** et en *italique* voir ~~barrés~~
> ````
> Mettre des mots en **gras** et en *italique* voir ~~barrés~~
> ````

* Faire des listes:
> * point 1
> * point 2
> 	* point 2.1
> 	* point 2.2
> 	
> 1. d'abord
> 2. ensuite
> 3. enfin
> ````
> * point 1
> * point 2
> 	* point 2.1
> 	* point 2.2
> 	
> 1. d'abord
> 2. ensuite
> 3. enfin
> ```

* insérer des liens vers [un super blog](https://archeomatic.wordpress.com/)
> ```
> insérer des liens vers [un super blog](https://archeomatic.wordpress.com/)
> ```

* ou insérer une image ![icon_gitlab](images/gitlab.png)
> ```
> ou insérer une image ![icon_gitlab](images/gitlab.png)
> ```

* et même des tableaux !
>| ID   | nom       | prénom | taille |
>| ---- | --------- | ------ | ------ |
>| 1    | la mouche | zobi   | 10     |
>| 2    | michel    | michel | 180    |
>```
>| ID   | nom       | prénom | taille |
>| ---- | --------- | ------ | ------ |
>| 1    | la mouche | zobi   | 10     |
>| 2    | michel    | michel | 180    |
>```

Un peu déconcertant les premières minutes... on se met très vite à prendre du plaisir à écrire et mettre en page juste avec un clavier !

Bon d'accord si vous insistez on peut mettre des émoticônes chat :cat:, voire des têtes de mort ! :skull: à vous de trouver comment [?!](https://gist.github.com/rxaviers/7360908)

## Pour aller plus loin

Ci-dessous quelques notes sur la syntaxe markdown [passer directement à la suite si vous êtes déjà convaincu ou que vous êtes préssés !](#range-ta-chambre!)
Outre la syntaxe que vous trouverez partout sur le web, ci-dessous quelques notes perso en plus... ↓

### caractères spéciaux (ASCII)

Tous les caractères spéciaux peuvent être écrit grace au ASCII. Il suffira en restant appuyé sur la touche [Alt] 
> par exemple `Alt+25`donnera ↓ et `Alt+3`donnera ♥
Vous trouverez les [codes ASCII](http://sebastienguillon.com/test/jeux-de-caracteres/windows-ascii-fr.html) sur la toile.

### insérer des liens 
Insérer un lien dans un texte en markdown est ridiculement simple, il suffit de l'écrire !
par exemple `https://archeomatic.wordpress.com` s'affichera https://archeomatic.wordpress.com
Pour ne pas surcharger le texte avec le lien on peut aussi écrire `[texte à afficher en bleu souligné](adresse_du_lien)`. ainsi `[mon blog](https://archeomatic.wordpress.com)` s'affichera comme ceci:  [mon blog](https://archeomatic.wordpress.com)

### Insérer des images
Insérer une image en markdown revient à insérer un lien vers une image il suffit de mettre
`![texte à afficher si pas d'image](lien_vers_l'image)`

> Les images utilisées peuvent soient déjà être en ligne soient stockées dans le dépôt.
>
> ![attention](images/attention.png) Le redimensionnement et la mise en page des images n'est pas gérée nativement avec le markdown il est fortement conseillé de préparer le images en amont !

### créer des liens dans un document
On peut avoir besoin de créer des liens à l'intérieur d'un document pour faire référence à un paragraphe dans une table des matières en début de document par exemple ou même pour mettre un peu de dynamisme dans le support (on pourrait même très simplement faire référence au résultat d'un exercice de cette façon.)

>  Rappel: En markdown les liens sont tous écrits sous la forme `[lien](adresse-du-lien)`.

Dans GitLab toutes les entêtes de paragraphe (celles qui sont précédées de 1 ou plusieurs #) obtiennent automatiquement un identifiant sous la forme `#mon-entete-en-minuscule-et-les-espaces-remplaces-par-un-tiret`.

La manière la plus simple pour faire référence à une entête paragraphe appelée **Introduction ** est donc d'y faire référence directement sous la forme `[lien vers l'intro](#Introduction)`. Ce qui donne [lien vers l'intro](#introduction)

Il faut respecter quelques règles:
- le lien est constitué d'un dièse `#`(quel que soit le niveau de titre un seul dièse suffit!)
- il n'y a pas d'espace entre le dièse et le nom
- le nom du titre doit être écrit en **minuscule** et les **espaces remplacés par un tiret `-`**
- Attention: les accents et caractères spéciaux ne sont pas pris en compte ! dans ce cas le plus simple est de récupérer le nom du lien dans gitlab par exemple.

> Pour récupérer la dénomination d'un lien dans GitLab il suffit de survoler l'entête dont on veut récupérer le lien , un icône lien apparaît alors sur la gauche de cette entête. Faire un clic droit => Copier l'adresse du lien. Puis coller le lien.
>
> ![lien](images/99_lien_interne.png)
>
> Le lien enregistré  et collé est : https://gitlab.inrap.fr/formation/perf/blob/master/memoSQL/memoSQL.md#requ%C3%AAte-dune-relation-de-1-%C3%A0-n
>
> Remarquez:
> - la mise en forme en minuscule
> - le remplace des espaces (mais aussi des `.`et des `&` s'il y en avait eut)
> - le remplacement de l'encodage des caractères accentués 
>   Note: au final on peut garder le lien en relatif  est supprimer toute la partie https://blabla
>   ainsi pour **faire référence à ce paragraphe spécifique** du document *memoSQL.md* contenu dans un dossier *[memosql]* depuis le fichier *README.md* à la racine du projet, il suffit d'écrire :
>   pour accéder directement au paragraphe sur les `[relations 1 à n](/memoSQL/memoSQL.md#requ%C3%AAte-dune-relation-de-1-%C3%A0-n)`
>   qui apparaîtra ainsi:
>   pour accéder directement au paragraphe sur les [relations 1 à n](/memoSQL/memoSQL.md#requ%C3%AAte-dune-relation-de-1-%C3%A0-n)



## Range ta chambre !

Dans ![gitlab](images/gitlab.png) toute la documentation écrite est au format **.md** dont le fichier central README.md qui s'affiche lorsqu'on accède au dépôt (et qui servira de page de garde à notre site web statique, on le verra plus tard..). 

Il est conseillé d’organiser son dépôt toujours de la même façon pour s'y retrouver:

1. Faire autant de dossier que d'ensemble cohérent de documents (par exemple un dossier par section/chapitre du support de formation ), lui donner **un nom court et explicite**
2. Dans ce [dossier] mettre les documents en **markdown** avec eux aussi des **noms courts, explicites et...ordonnés**, il est bienvenu par exemple de commencer par un chiffre: ```01_intro.md, 02_presentation_qgis, 03_edition```
3. Ce [dossier] contiendra aussi un sous-dossier [images] contenant toutes les images utilisées dans les documents du même dossier. Ainsi quel que soit le document en **markdown** du dépôt le chemin vers les images sera toujours ```[legende](images/nom_d_image.png)```
4. Enfin dernier conseil d'amis :smirk: : il peut être malin pour nommer les images de faire comme pour les documents.md c'est à dire leur donner des **noms courts, explicites et...ordonnés !**. Rappellez vous que le chemin des images sera tapé à la main...



# Mise en ligne d'un site statique d'après des documents en markdown stockés sur gitlab

Vous avez déjà créé/alimenté un dépôt gitlab avec des tonnes de documents.md, le tout bien rangé ! Il est temps de passer aux choses sérieuses : mettre en valeur ce travail magnifique ! Le mettre en ligne dans un site web *dit* statique. 

Ceci est un ensemble de notes pour tenter une mise en ligne sous forme de site web statique avec le systeme de Gitlab Pages et [gitbook]()

Ressources:
*  [documentation de GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
* [publier un website avec GitLab Pages (video eng)](https://youtu.be/TWqh9MtT4Bg)

## Créer le site web statique
Le plus simple -quand on part de Zéro- est de partir d'un exemple que l'on *fork* (on en crée une  fourche, une branche) comme [proposé (en anglais) dans la documentation de GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/getting_started/fork_sample_project.html): 

1. Faire un fork du projet modèle:

   * se rendre sur la [page 2 des exemples de Pages GitLab](https://gitlab.com/pages?page=2)
   * cliquer sur **gitbook**
   * cliquer sur le bouton ![fork](images/bouton_fork.png)pour faire un fork du projet exemple et sélectionner votre espace de noms (vous vous êtes identifié sur gitlab auparavant bien sûr!)
   * Bravo vous avez dorénavant un projet **gitbook** dans votre espace GitLab !

2. Casser la relation entre le projet "forké" et le projet principal:

   * Une fois dans le projet: Projet > Paramètres > Général
   * Dérouler la page jusqu'à la rubrique **Advanced** et cliquer sur [Etendre]
   * Dérouler la page jusqu'à [Remove fork relationship] et cliquer sur le bouton idoine.

   >  note: le logiciel me demande de confirmer l'action en tapant `gitbook` puis [Confirm]

   * Une fois revenu sur le haut de la page des paramètres généraux du projet donner un nom au projet si vous voulez en changer (pour l'exemple je garde gitbook)

3. Intégration et livraison continue (CI/CD en anglais) Pipelines > [Enable shared runners] 

   > Note: A ne faire qu'une fois, la 1ère fois que l'on fait un site..

1. Pour tester modifier le fichier Readme.md en ajoutant Hello world ! dans les premières lignes.

2. Intégration et livraison continue > Pipelines > cliquer sur le Bouton [Pages]

3. Aller dans les Paramètres > Pages et regarder l'url de votre page web statique: tester 

   > **Attention** la première fois cela peut mettre plusieurs minutes  -une 10aine pour moi- avant de voir la page !

## Paramétrer le site avec votre nom  d'utilisateur: moi.gitlab.io/mon_projet

1. Renommer le dépôt (repository)

   * Paramètres du projet > Rename Repository
   * En profiter pour renommer le projet lui-même
   * Retourner sur Paramètres > pages et vérifier la nouvelle url

2. Recréer le lien vers le css

   * Éditer le fichier .config.yml
   * à la ligne baseurl: "blablabla" supprimer blablabla
   * Actualiser la page web !

## Ce qu'il faut retenir

   Pour faire un site web statique, il faut obligatoirement certains **fichiers à la racine du projet**:

   > Note: PAS DE PANIQUE, il  suffit la plupart du temps de récupérer des fichiers existant et éventuellement de changer quelques paramètres.
   >
   > Note: certains fichier existent déjà dans le projet 

   * un fichier **.gitlab-ci.yml** (langage html) qui sera le fichier qui générera le site statique.

   >  C'est dans ce fichier  sous forme de script que l'on indique à gitlab les actions à effectuer (par exemple: l’installation de l'extension gitbook, et la construction du siteweb statique avec ce dernier mais aussi la création d'un pdf). 
   >
   > On peut voir ce fichier comme un manifeste une sorte de feuille de route pour l’exécution de tâches reliées entre elles dans un *pipeline*. Cette suite d'action sera effectuée à chaque *commit* (changement dans le projet) .

   *  un fichier **.gitignore** qui contient les fichiers qui doivent être ignorés lors des *commit*

   > On va par exemple lui dire d'ignorer les pdf ou les fichiers qu'il a lui même créés auparavant.

   *  un fichier **README.md** qui existe déjà mais qui a avoir ici aussi la fonction de page de garde du site web statique.

   > Ce fichier existe déjà il aura juste une fonction supplémentaire.

   *  un fichier **SUMMARY.md**  qui comme son nom l'indique contient le sommaire sous forme de liste avec des liens vers les documents .md a rassembler sur le site web.

   En outre, il faut activer **l'intégration et livraison continue** plus communément appelée **CI/CD** (*Continuous Integration / Continuous Delivery*) qui est le service qui va a chaque changement exécuter les ordres 

   > Tout ceci se fait dans le projet via le menu ![integration_et_livraison_pipelines](images/06_pipeline.png)
   >
   > La première fois  il faut activer/déployer la tâche "Pages" en cliquant sur ![icon_pipeline](images/icon_pipeline.png)Pages.
   >
   > 

   *  CI/CD Pipelines il faut activer/déployer la tâche "Pages"
   * l'adresse du site est https://username.gitlab.io/mon_projet_de_website