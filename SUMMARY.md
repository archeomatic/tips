# Summary

## SIG perfectionnement 1: Analyses Spatiales

* [Utiliser GitLab comme support de formation](tuto_gitlab/0_guid_form_git.md)
* [Ecrire un document en Markdown](tuto_gitlab/1_guid_form_markdown.md)
* [L'essentiel pour débuter avec git](tuto_gitlab/2_guid_form_git_initiation.md)
* [Bonus: Mise en ligne d'un site web statique](tuto_gitlab/3_guid_form_gitlabpages.md)