## Exemple de Diaporama
* 100 % Markdown (ou presque)
* propulsé par Gitlab
* généré automatiquement à chaque `git push`

---

## Comment naviguer dans le diaporama

| Key                           | Action                                    |
| ----------------------------- | ----------------------------------------- |
| <kbd>S</kbd>                  | Open Speaker view                         |
| <kbd>F</kbd>                  | Full screen / <kbd>Esc</kbd> to exit      |
| <kbd>B</kbd>                  | Break = pause presentation = black screen |
| <kbd>Esc</kbd> / <kbd>O</kbd> | Toggle overview mode = zoom out           |
| <kbd>Alt</kbd>+`Click`        | Zoom in                                   |
| <kbd>Space</kbd>              | Next slide                                |



Note: pas d'indication de background dans le markdown → celui paramétré dans index.html

--

## Le fichier example.md
<!-- .slide: data-background="#cfe2f3" -->

Dans le fichier **example.md**  on peut utiliser la syntaxe ***Markdown***.
En outre il faudra:

* Annoncer le changement de diapo avec la chaîne de carcatère choisie (ici 3 tirets encadrés par 1 saut de ligne au dessus et au dessus pour passer à la diapo suivante à droite & même chose avec 2 tirets pour passer à la diapo suivante en dessous) 
<!-- .element: class="fragment" data-fragment-index="1" -->

* éventuellement ajouter des paramètres à l'aide de commentaires significatifs: 
*exemple:* pour changer la couleur de fond de cette diapo en bleu on ajoutera:
```css
<!-- .slide: data-background="#cfe2f3" -->
```
<!-- .element: class="fragment" data-fragment-index="2" -->

Note: This will only appear in the speaker notes window.

---

## Mardown: Titres et emphases
```markdown
### Titre de niveau 3
#### Titre de niveau 4 
```
donne:
### Titre de niveau 3
#### Titre de niveau 4

Et les emphases:
```markdown
**gras**
*italique*
~~barré~~
```
donne:

**gras**

*italique*

~~barré~~

--

## Markdown: les listes

On peut utiliser les **listes**:

* item 1 <!-- .element: class="fragment" data-fragment-index="1" -->
* item 2 <!-- .element: class="fragment" data-fragment-index="2" -->
	* item 2.1
	* item 2.2
		* item 2.2.1
* item 3 <!-- .element: class="fragment" data-fragment-index="3" -->

--

##  Markdown: Des images

<!-- .slide: data-background="#ffffff" -->

![test](images/cover.png)

un document sous licence ![cc](images/CC.png), évidemment !
